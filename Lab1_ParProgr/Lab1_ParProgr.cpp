

#include "stdafx.h"
#include "mpi.h"
#include <iostream>
#include <stdio.h>

int _tmain(int argc, char * argv[]) {

	int rank, num = INT_MAX, resultLength, size;
	char name[MPI_MAX_PROCESSOR_NAME];

	MPI_Init(&argc, &argv); 
	MPI_Comm_size(MPI_COMM_WORLD, &size);  
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);  
	do {
		if (rank == 0) { 
			MPI_Barrier(MPI_COMM_WORLD);
			std::cout << "Enter only the number (0 or less to exit):\n";
			std::cin >> num;
		}
		else { MPI_Barrier(MPI_COMM_WORLD); }
		MPI_Bcast(&num, 1, MPI_INT, 0, MPI_COMM_WORLD);
		MPI_Get_processor_name(name, &resultLength);
		MPI_Barrier(MPI_COMM_WORLD);
		printf("Process %d of %d at %s got: %d\n", rank, size, name, num);
		fflush(stdout);
	} while (num > 0);
	MPI_Finalize();
}

